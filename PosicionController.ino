/*  La idea es controlar las velocidad de ambos motores , pero
    en el modelo global vamos a controlar es la velocidad
    lineal de toda la planta y la velocidad angular V y  W
    para posterirormente llevarlo a una posicion del espacio
    vamos a poner los rpm en funcion de las velocidad lineal
    de cada una de las llantas para asi poder aplicar las
    ecuaciones del modelo
    MAXIMO RPM = 80
    MAXIMO FRENCIA = 1.3333 Hz
    MAXIMA W = 4.42 rad/s
    MAXIMO V = 50.2 cm/s
*/
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <i3Motor.h>
i3Motor carrito(7, 6, 5, 4); // se escogen los pines
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

#define vtmax 20.000
#define wmax 3.400

double ref_1 = 0, ref_2 = 0; // ref_1erencia de --- rpm_1

const int sample_sensor = 50; // tiempo de muestreo de los pulsos para saber rpm_1 ---ms
const double R = 6.2;    //cm radio de la llanta
const int ranuras  = 1625.000;// resolucion del encoder
const double L = 22.7; //cm La distancia que hay entre las llantas
const double Retraso = 0.0;// Retardo natural del micro al hacer las instrucciones Â¡MUY IMPORTANTE!
const double a = 7.0000; //  a cm desplazado del centro del carro

unsigned long tiempo = 0, tiempoAnt = 0, tiempoD = 0, tiempoI = 0, tiempoX = 0, tiempoY = 0, tiempoW = 0;

const int maxStep = 255; // maxima velocidad 2^8 arduino 8 bits
volatile int ISRCounter = 0, ISRCounter_2 = 0;

// Variables del controlador de PID //
double rpm_1 = 0, rpm_1_pass = 0 , rpm_2 = 0, rpm_2_pass = 0;
double error_1 = 0, error_2 = 0;
double kp_1 = 3.5, ki_1 = 0.08, kd_1 = 3, kp_2 = 3.3, ki_2 = 0.08, kd_2 = 3;
double derivativa_1 = 0, integrativa_1 = 0, derivativa_2 = 0, integrativa_2 = 0;

double Vt = 0; // Velocidad lineal del sistema completo que deseamos [cm/s] TEORICA
double W =  0; // Frecuencia angular que deseamos [rad/s] TEORICA


double Vtr = 0; // Velocidad lineal del sistema completo que deseamos [cm/s] REAL
double Wr =  0; // Frecuencia angular que deseamos [rad/s] REAL

// Variables de posicion //
double angulo = 0, angulo_grados, xc = 0, yc = 0;
double xd = xc + (a * cos(angulo)); // Posicion en x desplazada a cm
double yd = yc + (a * sin(angulo)); // Posicion en y desplazada a cm

// Posicion Deseada //
double xd_deseado = 0, yd_deseado = 0;

// Derivada de la posicion deseada //
double xdp = 0, ydp = 0;

long double t = 0;
bool sentido_1 = 0, sentido_2 = 0;

int Ruta = 0;
#define tam 4
double Coordenadas[tam][3] = {
  {180, 0},
  {180, 120},
  {120, 0},
  {0, 0},
};
void setup()
{
  Serial.begin(9600);
  lcd.begin(20, 4);

  attachInterrupt(digitalPinToInterrupt(15), MOTOR_1 ,  RISING);
  attachInterrupt(digitalPinToInterrupt(17), MOTOR_2 ,  RISING);
}
void loop()
{
  //  xd_deseado = Coordenadas[Ruta][0];
  //  yd_deseado = Coordenadas[Ruta][1];
  double k = 4.0000;
  xd_deseado = ((45.000 * k * sin(0.1 * t)) - (15.000 * k * sin(3.000 * 0.1 * t))) / 4.000;
  yd_deseado = ((13.000 * k * cos(0.1 * t)) - (5.000 * k * cos(2.000 * 0.1 * t)) - (2.000 * k * cos(3.000 * 0.1 * t)) - (k * cos(4.000 * 0.1 * t)) / 4.000);
  xdp = ((45.000 * k * 0.1 * cos(0.1 * t)) - (15.000 * 3.000 * 0.1 * k * cos(3.000 * 0.1 * t))) / 4.000;
  ydp = ((-13.000 * 0.1 * k * sin(0.1 * t)) - (-5.000 * 2.000 * 0.1 * k * sin(2.000 * 0.1 * t)) - (-2.000 * 3.000 * 0.1 * k * sin(3.000 * 0.1 * t)) - (-k * sin(4.000 * 0.1 * t)) / 4.000);


  tiempo = millis();
  if (tiempo - tiempoAnt >= sample_sensor) {
    detachInterrupt(digitalPinToInterrupt(15));
    detachInterrupt(digitalPinToInterrupt(17));

    v_rpm(); // Convertimos la velocidad lineal(cm/s) y  W en rpm
    SensarVelocidades(); //Sensamos la velocidad de cada motor
    Jacobiana(0.6, 0.6); // Controlamos V y W
    Posicion();//Extraemos las coordenadas

    attachInterrupt(digitalPinToInterrupt(15), MOTOR_1 ,  RISING);
    attachInterrupt(digitalPinToInterrupt(17), MOTOR_2 ,  RISING);

    tiempoAnt = tiempo;
  }
  Controlador(); // Controlamos la velocidad deseada de cada motor

  lcd.setCursor(0, 0);
  lcd.print(String("x:") + (int)xd + String(" y:") + (int)yd);

  Serial.println(Vt + String(",") + Vtr + String(",") + (10.000 * W) + String(",") + (10.000 * Wr));
}
void SensarVelocidades() {

  t += 0.07;

  rpm_1 = (double)((ISRCounter / ((millis() -  tiempoD) / 1000.0000)) * (60.000000 / ranuras));
  rpm_2 = (double)((ISRCounter_2 / ((millis() - tiempoI) / 1000.0000)) * (60.000000 / ranuras));

  tiempoD = millis();
  tiempoI = millis();
  ISRCounter = 0;
  ISRCounter_2 = 0;

}
void MOTOR_1()
{
  ISRCounter++;
}

void MOTOR_2()
{
  ISRCounter_2++;
}
void Controlador() {
  int maX = 1000, miN = -1000;
  double output_1 = 0, output_2 = 0;
  double h1 = 0.1, h2 = 0.1;
  // ref siempre es positivo //
  error_1 = (ref_1 - rpm_1) * kp_1;
  error_2 = (ref_2 - rpm_2) * kp_2;

  derivativa_1 = (rpm_1 - rpm_1_pass) * kd_1;
  derivativa_2 = (rpm_2 - rpm_2_pass) * kd_2;

  // Esto condicionales nos ayudan a eliminar el sobrepaso de la respuesta del sistema //
  if ((derivativa_1 == 0.0) || (error_1 = 0.0)) integrativa_1 += (error_1 * ki_1); else integrativa_1 -= (derivativa_1 * ki_1);
  if ((derivativa_2 == 0.0) || (error_2 = 0.0)) integrativa_2 += (error_2 * ki_2); else integrativa_2 -= (derivativa_2 * ki_2);

  if (integrativa_1 > maX) integrativa_1 = maX; else if (integrativa_1 < miN) integrativa_1 = miN;
  if (integrativa_2 > maX) integrativa_2 = maX; else if (integrativa_2 < miN) integrativa_2 = miN;

  output_1 = error_1 + integrativa_1 - derivativa_1 + h1 * output_2;
  output_2 = error_2 + integrativa_2 - derivativa_2 + h2 * output_1;

  if (output_1 > maxStep) output_1 = maxStep; else if (output_1 < -maxStep)output_1 = - maxStep;
  if (output_2 > maxStep) output_2 = maxStep; else if (output_2 < -maxStep)output_2 = - maxStep;

  //Aplicamos accion de control //
  carrito.Motores(output_1, output_2, sentido_1, sentido_2);

  rpm_1_pass = rpm_1;
  rpm_2_pass = rpm_2;

}
void v_rpm() { // Calculamos las velocidad de los motores en funcion de v y w
  double v1, v2;

  // hp = J*V despejamo la matriz V = inv(J)*(hp + Ke)
  // Donde e es la matriz de errores y K una matriz diagonal de ganancias

  v1 = Vt + ((W * L) / (2.0000));// Velocidades deseadas
  v2 = Vt - ((W * L) / (2.0000)); // Velocidades deseadas

  if (v1 < 0 ) sentido_1 = 1; else sentido_1 = 0;
  if (v2 < 0) sentido_2 = 1; else sentido_2 = 0;

  // Debe ser siempre positiva las referencias
  ref_1 = abs(((v1) / (2.0000 * PI * R)) * (60.0000));
  ref_2 = abs(((v2) / (2.0000 * PI * R)) * (60.0000));
}
void Posicion() {

  // Calculamos Vt real y W real //

  double v1r = ((2.000 * R * PI * rpm_1) / (60.000));
  double v2r = ((2.000 * R * PI * rpm_2) / (60.000));

  if (sentido_1) v1r = -1.000 * v1r; else v1r = abs(v1r);
  if (sentido_2) v2r = -1.000 * v2r; else v2r = abs(v2r);

  Vtr = (v1r + v2r) / (2.000);
  Wr = (v1r - v2r) / (L);

  angulo += double(Wr * ((((millis() - tiempoW)) / 1000.0000) + Retraso));
  tiempoW = millis();

  angulo_grados = angulo * (180.0000 / PI);

  double xpd = (Vtr * cos(angulo)) - (a * Wr * sin(angulo)); // velocidad en x
  double ypd = (Vtr * sin(angulo)) + (a * Wr * cos(angulo)); // velocidad en y

  xd += (xpd * ((millis() - tiempoX) / 1000.0000)); // La integral de la velocidad en x
  yd += (ypd * ((millis() - tiempoY) / 1000.0000)); // La integral de la velocidad en y

  tiempoX = millis();
  tiempoY = millis();

  xc = xd - (a * cos(angulo));
  yc = yd - (a * sin(angulo));
}
void Jacobiana(double k1, double k2) {
  double e1 = xd_deseado - xd;
  double e2 = yd_deseado - yd;

  if ((abs(e1) < 1) && (abs(e2) < 1))Ruta++;
  if (Ruta > tam - 1) Ruta = 0;

  Vt = (((e1 * k1) + xdp) * cos(angulo)) + (((k2 * e2) + ydp) * sin(angulo)); // Velocidad lineal teorica necesario
  W =  ((-1.000 / a) * ((e1 * k1) + xdp) * sin(angulo)) + ((1.000 / a) * ((k2 * e2) + ydp) * cos(angulo));// Velocidad angular teorica necesaria

  // Limites maximos obtenidos a 80 rpm //
  if (Vt > vtmax) Vt = vtmax; else if (Vt < -vtmax) Vt = -vtmax;
  if (W > wmax) W = wmax; else if (W < -wmax) W = -wmax;

}
